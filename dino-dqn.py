
# coding: utf-8

# In[1]:


import gym
import gym_chrome_dino
from gym_chrome_dino.utils.wrappers import make_dino


# In[2]:


done = False


# In[ ]:


from agent import AGENT
TOTAL_EPISODES = 10000
agent = AGENT(total_episodes=TOTAL_EPISODES)


# In[ ]:


for episode in range(TOTAL_EPISODES):
    env = gym.make('ChromeDinoNoBrowser-v0')
    env = make_dino(env, timer=True, frame_stack=True)
    observation = env.reset()
    agent.curr_episode = episode + 1
    done = False
    
    while not done:
        action = agent.choose_action(observation=observation)
        observation_, reward, done, info = env.step(action)
        agent.save_this_experience(observation=observation, action=action, reward=reward, observation_=observation_)
        observation = observation_
    
    score = env.unwrapped.game.get_score()
    agent.post_episode_task(episode_score=score)
    print("Episode #", (episode + 1), score)

